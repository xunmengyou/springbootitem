/**
 * 
 */
package cn.wwy.service;

import cn.wwy.service.impl.CityServiceImpl;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cn.wwy.common.BasicTest;
import cn.wwy.domain.City;

/**
 * @author wangwy
 *
 */
public class CityServiceImplTest extends BasicTest{
	
	@Autowired 
	private CityServiceImpl citySe;
		
    @Test //City findCityByName(String cityName)
    public void findCityByNameTest() {
    	City city=citySe.findByName("wl");
    	System.out.println(city.getProvinceId());
    }
}
