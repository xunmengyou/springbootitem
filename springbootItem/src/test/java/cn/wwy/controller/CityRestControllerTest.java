/**
 * 
 */
package cn.wwy.controller;

import org.junit.Before;
import org.junit.Test;

import org.hamcrest.Matchers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import cn.wwy.common.BasicTest;

/**
 * @author wangwy
 *
 */
public class CityRestControllerTest extends BasicTest{
	
	private MockMvc mockMvc;  // 模拟MVC对象，通过MockMvcBuilders.webAppContextSetup(this.wac).build()初始化。    
	
	@Autowired
	 private WebApplicationContext wac; // 注入WebApplicationContext    

//  @Autowired    
//	private MockHttpSession session;// 注入模拟的http session    
    
//  @Autowired    
//	private MockHttpServletRequest request;// 注入模拟的http request\    
	@Before // 在测试开始前初始化工作    
	public void setup() {    
	    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();    
	}    

	@Test
	public void findOneCityTest() throws Exception {
//		 mockMvc.perform(MockMvcRequestBuilders.get("/api/city")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .param("cityName", "wl")
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.status().isOk())
//                .andDo(MockMvcResultHandlers.print())
//                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("SUCCESS")));

	MvcResult result =	mockMvc.perform(MockMvcRequestBuilders.get("/api/city")
      .contentType(MediaType.APPLICATION_JSON_UTF8)
      .param("cityName", "wl")
      .accept(MediaType.APPLICATION_JSON))
      .andExpect(MockMvcResultMatchers.status().isOk())
      .andReturn();
	
	System.out.println(result.getResponse().getContentAsString());
	}

}
