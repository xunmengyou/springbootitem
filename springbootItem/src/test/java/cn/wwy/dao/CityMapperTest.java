/**
 * 
 */
package cn.wwy.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cn.wwy.common.BasicTest;
import cn.wwy.domain.City;
/**
 * @author wangwy
 *
 */

public class CityMapperTest extends BasicTest{
	@Autowired
	 private CityMapper citydao;
	
	@Test  //City findByName(@Param("cityName") String cityName);
	public void testfindByName() {
		City city=citydao.findByName("wl");
		System.out.println(city.getId());
	}

	
}
