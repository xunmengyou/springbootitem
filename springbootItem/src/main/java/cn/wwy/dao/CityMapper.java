/**
 * 
 */
package cn.wwy.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.wwy.domain.City;
import org.springframework.stereotype.Repository;

/**
 * @author wangwy
 *
 */
//@Repository
public interface CityMapper {
	City findByName(@Param("cityName") String cityName);
}
