package cn.wwy.domain;
import java.io.Serializable;
import java.util.Date;

/**
 * @author wangwy
 *
 */
public class Log implements Serializable{
	 private static final long serialVersionUID = 1L;
	 private Integer id;
	 private Date createTime;
	 private Integer userId;
	 private String userName;
	 private String requestIp;
	 private String eventType;
	 private String operateDescription;
	 private String detail;
	 private Integer viewLevel;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRequestIp() {
		return requestIp;
	}
	public void setRequestIp(String requestIp) {
		this.requestIp = requestIp;
	}
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getOperateDescription() {
		return operateDescription;
	}
	public void setOperateDescription(String operateDescription) {
		this.operateDescription = operateDescription;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public Integer getViewLevel() {
		return viewLevel;
	}
	public void setViewLevel(Integer viewLevel) {
		this.viewLevel = viewLevel;
	}

}
