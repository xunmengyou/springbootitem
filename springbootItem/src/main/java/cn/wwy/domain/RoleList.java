/**
 * 
 */
package cn.wwy.domain;

import java.io.Serializable;
import java.util.List;

/**
 * @author wangwy
 *
 */
public class RoleList implements Serializable{
	private String roleId;   //角色Id
	private String roleName;  //角色名
	private String description;  //描述
	private String createBy;  //创建人(用户)
	private String type;
    private List<PermissionInfo> listPerInfo;
    
    public String getRoleId()
    {
    
        return roleId;
    }
    public void setRoleId(String roleId)
    {
    
        this.roleId = roleId;
    }
    public List<PermissionInfo> getListPerInfo()
    {
    
        return listPerInfo;
    }
    public void setListPerInfo(List<PermissionInfo> listPerInfo)
    {
    
        this.listPerInfo = listPerInfo;
    }
    
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}  
	
}
