package cn.wwy.common.constant;

import java.util.List;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;

import cn.wwy.common.entity.ReturnResult;
/**
 * 
 * Json工具
 * json与对象之间的互相转换  
 * @author wuhx  
 * @version 2018年5月21日  
 * @see JsonUtil  
 * @since
 */
public class JsonUtil {
	/**
     * 序列化参数
     */
    private static final SerializerFeature[] FEATURES = {
            SerializerFeature.WriteMapNullValue,
            SerializerFeature.WriteNullBooleanAsFalse,
            SerializerFeature.WriteNullStringAsEmpty,
            SerializerFeature.WriteNullListAsEmpty,
            SerializerFeature.WriteNullNumberAsZero };

    /**
     * 
     * Description:    
     * 1、对象转换成json 支持list,map,array
     * @param obj 待转换对象
     * @return String 转换后的json
     */
    public static String objToJson(Object obj) {
        return JSON.toJSONString(obj, FEATURES);
    }
    
    public static <T> List<T> jsonToList(String json,Class<?> clazz){
        return (List<T>)JSONObject.parseArray(json, clazz);
    }


    /**
     * 
     * Description:    
     * 1、json转换成对象
     * @param json 目标json
     * @param clazz 目标类型
     * @return T 转换后的对象，根据目标类型而变化
     */
    public static <T> T jsonToObj(String json, Class<?> clazz) {
        return (T) JSON.parseObject(json, clazz);
    }
    /**
     * 
     * Description:    
     * 1、将控制器的返回结果封装到ReturnResult实体类，并转换为Json格式传给前台
     * @param code 状态码
     * @param msg 提示信息
     * @param data 返回数据，可为null
     * @return  
     * @author:	wuhx  
     * @date:	2018年5月31日 下午2:49:08
     */
    public static String resultToJson(String code, String msg, Object data) {
        ReturnResult result = new ReturnResult();
        result.setCode(code);
        result.setMsg(msg);
        result.setData(data);
        return objToJson(result);
    }

}
