package cn.wwy.common.constant;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 网络工具类  
 * 与网络操作有关的通用方法  
 * @author wuhx  
 * @version 2018年5月21日  
 * @see NetworkUtil  
 * @since
 */
public class NetworkUtil {
	
    /**
     * 
     * Description:    
     * 1、获得来自远程访问的IP地址
     * @param request 访问请求
     * @return String 远程主机的IP地址
     */
	public static String getRemoteHost(HttpServletRequest request){
	    String ip = request.getHeader("x-forwarded-for");
	    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
	        ip = request.getHeader("Proxy-Client-IP");
	    }
	    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
	        ip = request.getHeader("WL-Proxy-Client-IP");
	    }
	    if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
	        ip = request.getRemoteAddr();
	    }
	    return "0:0:0:0:0:0:0:1".equals(ip)?"127.0.0.1":ip;
	}
	
	/**
	 * 
	 * Description:    
	 * 1、获得Json字符串
	 * @param request
	 * @return  
	 * @author:	wangwy  
	 * @date:	2018年5月30日 下午1:54:09
	 */
    public static JSONObject getJSONParam(HttpServletRequest request)
    {
        JSONObject jsonParam = null;
        try
        {
            // 获取输入流
            BufferedReader streamReader = new BufferedReader(
                new InputStreamReader(request.getInputStream(), "UTF-8"));

            // 写入数据到Stringbuilder
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = streamReader.readLine()) != null)
            {
                sb.append(line);
            }
            jsonParam = JSONObject.parseObject(sb.toString());
            // 直接将json信息打印出来
            // System.out.println(jsonParam.toJSONString());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return jsonParam;
    }
}
