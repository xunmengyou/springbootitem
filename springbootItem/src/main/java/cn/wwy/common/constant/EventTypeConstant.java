
/**  
 * @Title:  EventTypeConstant.java   
 * @Package com.infinova.oms.common.constants   
 * @Description:	TODO(用一句话描述该文件做什么)   
 * @author: wuhx     
 * @date:   2018年5月30日 下午3:20:28   
 */
  
package cn.wwy.common.constant;  
  /**
   * 
   * 类功能简述:	日志事件类型常量，开发者写日志注解时必须从这里面选择
   * 类功能详述:	
   * @author:	wuhx  
   * @date:	2018年5月30日 下午3:22:15
   */
public interface EventTypeConstant
{
    String ADD_DICTIONARY = "ADD_DICTIONARY";
    String UPDATE_DICTIONARY = "UPDATE_DICTIONARY";
    String DELETE_DICTIONARY = "DELETE_DICTIONARY";
    //用户管理
    String ADD_USER = "ADD_USER";
    String UPDATE_USER = "UPDATE_USER";
    String DELETE_USER = "DELETE_USER";
    String USER_UPDATE_PWD = "USER_UPDATE_PWD";
    //角色管理
    String ADD_ROLE = "ADD_ROLE";
    String UPDATE_ROLE = "UPDATE_ROLE";
    String DELETE_ROLE = "DELETE_ROLE";
    String UPDATE_ROLE_AUTHORITY = "UPDATE_ROLE_AUTHORITY";
    
    String ADD_ALARMRULE = "ADD_ALARMRULE";
    String UPDATE_ALARMRULE = "UPDATE_ALARMRULE";
    String DELETE_ALARMRULE = "DELETE_ALARMRULE";
}

