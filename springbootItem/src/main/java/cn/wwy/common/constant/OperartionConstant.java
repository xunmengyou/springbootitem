
 /*  
 * 文件名：OperartionConstant.java  
 * 版权：
 * 描述：  
 * 修改人：wuhx  
 * 修改时间：2018年5月23日  
 * 跟踪单号：  
 * 修改单号：  
 * 修改内容：  
 */  
  
package cn.wwy.common.constant;  
/**
 *   
 * 操作提示常量接口 
 * @author wuhx  
 * @version 2018年5月24日  
 * @see OperartionConstant  
 * @since
 */
public interface OperartionConstant
{
    String ADD_SUCCESS = "添加成功";
    String ADD_FAILURE = "添加失败";
    String DELETE_SUCCESS = "删除成功";
    String DELETE_FAILURE = "删除失败";
    String PASSWORD_LEVEL = "passwordLevel";
    String SUCCESS = "SUCCESS";
    String FAILURE = "FAILURE";
    String ARG_ERROR = "参数错误";
    String UPDATE_SUCCESS = "修改成功";
    String UPDATE_FAILURE = "修改失败";
    
    String USER_EXIST = "用户已存在";
    String USER_ADD_ERROR = "添加用户错误";
    String USER_ADD_ROLE_ERROR = "添加用户角色错误";
    /*
     * wwy
     */
    Integer PAGE_ROW = 1;
    Integer PAGE_SIZE = 20;
    Integer MAX_QUERY_LOG_SIZE = 10000;
}

