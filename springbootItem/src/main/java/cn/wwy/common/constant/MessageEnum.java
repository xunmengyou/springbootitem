
/**  
 * @Title:  MessageEnum.java   
 * @Package com.infinova.oms.common.constants   
 * @Description:	TODO(用一句话描述该文件做什么)   
 * @author: yizl     
 * @date:   2018年6月21日 下午3:16:02   
 */
  
package cn.wwy.common.constant;  
/**
 *  
 * 类功能简述:	〈一句话）
 * 类功能详述:	
 * @author:	yizl  
 * @date:	2018年6月22日 上午9:17:04
 */
public enum MessageEnum {

    SUCCESS("成功", 1000),
    ERROR("失败", -1000);
    
    public final String MSG;
    public final int CODE;
    
    private MessageEnum(String msg, int code) {
        this.MSG = msg;
        this.CODE = code;
    }

    public String getMSG()
    {
    
        return MSG;
    }

    public int getCODE()
    {
    
        return CODE;
    }

}

