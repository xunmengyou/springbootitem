package cn.wwy.common.entity;

import java.util.Date;
import java.util.List;

public class Role {

	private String id;
	
	private String lastModifyUId;

	private Date lastModifyTime;

	private String roleName;

	private String description;
	// 角色类型;-1,超级管理员,;0:一般角色;1:管理员
	private int type;
	// 1:使用;-1:删除
	private int status;
	private List<Permission> permissions;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLastModifyUId() {
		return lastModifyUId;
	}

	public void setLastModifyUId(String lastModifyUId) {
		this.lastModifyUId = lastModifyUId;
	}

	public Date getLastModifyTime() {
		return lastModifyTime;
	}

	public void setLastModifyTime(Date lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Permission> getPermissions() {
		return permissions;	
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}

}
