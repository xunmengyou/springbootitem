
/**  
 * @Title:  PermissionRelationship.java   
 * @Package com.infinova.oms.common.entity   
 * @Description:	TODO(用一句话描述该文件做什么)   
 * @author: wangwy     
 * @date:   2018年6月1日 上午10:25:34   
 */
  
package cn.wwy.common.entity;

import java.io.Serializable;

/**  
 * 类功能简述:	〈一句话）
 * 类功能详述:	权限树的信息
 * @author:	wangwy  
 * @date:	2018年6月1日 上午10:25:34 
 */

public class PermissionRelationship implements Serializable
{
    private String id;  //id
    private String pid;  //父id
    private String permissionName;  //权限名
    private String type;  //类型
    private String url;
    private String seq;  //排序
    private String operationKey; //对页面的操作key
    private int hasRole; //该角色拥有该权限,1表拥有,0表示不拥有
    
    public String getId()
    {
    
        return id;
    }
    public void setId(String id)
    {
    
        this.id = id;
    }
    public String getPid()
    {
    
        return pid;
    }
    public void setPid(String pid)
    {
    
        this.pid = pid;
    }
    public String getType()
    {
    
        return type;
    }

    public void setType(String type)
    {
    
        this.type = type;
    }

    public String getUrl()
    {
    
        return url;
    }

    public void setUrl(String url)
    {
    
        this.url = url;
    }

    public String getSeq()
    {
    
        return seq;
    }

    public void setSeq(String seq)
    {
    
        this.seq = seq;
    }

    public int getHasRole()
    {
    
        return hasRole;
    }

    public void setHasRole(int hasRole)
    {
    
        this.hasRole = hasRole;
    }

    public String getPermissionName()
    {
    
        return permissionName;
    }

    public void setPermissionName(String permissionName)
    {
    
        this.permissionName = permissionName;
    }

    public String getOperationKey()
    {
    
        return operationKey;
    }

    public void setOperationKey(String operationKey)
    {
    
        this.operationKey = operationKey;
    }
    
}

