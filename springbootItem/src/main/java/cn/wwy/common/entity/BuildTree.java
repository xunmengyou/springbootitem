
/**  
 * @Title:  BuildTree.java   
 * @Package com.infinova.oms.common.entity   
 * @Description:	TODO(用一句话描述该文件做什么)   
 * @author: wangwy     
 * @date:   2018年6月1日 上午10:43:06   
 */
  
package cn.wwy.common.entity;

import java.util.ArrayList;
import java.util.List;


/**  
 * 类功能简述:	〈一句话）
 * 类功能详述:	
 * @author:	wangwy  
 * @date:	2018年6月1日 上午10:43:06 
 */

public class BuildTree
{
    /**
     * 
     * Description:    
     * 1、构建相应的树信息
     * @param nodes
     * @return  
     * @author:	wangwy  
     * @date:	2018年6月1日 上午10:53:50
     */
   public static <T> Tree<T> build(List<Tree<T>> nodes){
       if(nodes == null){
           return null;
         }
       List<Tree<T>> topNodes = new ArrayList<Tree<T>>();
       
       for (Tree<T> children : nodes) {
    
         String pid = children.getParentId();
         if (pid == null || "".equals(pid)) {
           topNodes.add(children);
           continue;
         }
    
         for (Tree<T> parent : nodes) {
           String id = parent.getId();
           if (id != null && id.equals(pid)) {
             parent.getChildrens().add(children);
             children.setParent(true);
             parent.setChildren(true);
              
             continue;
           }
         }
    
       }
    
       Tree<T> root = new Tree<T>();
       if (topNodes.size() == 0) {
         root = topNodes.get(0);
       } else {
        //
       }    
       return root;
   }
   
   public static <T> List<Tree<T>> buildList(List<Tree<T>> nodes) {
       
       if(nodes == null){
           return null;
         }
         List<Tree<T>> topNodes = new ArrayList<Tree<T>>();
      
         for (Tree<T> children : nodes) {
      
           String pid = children.getParentId();
           if (pid == null || "".equals(pid)) {
             topNodes.add(children);
      
             continue;
           }
      
           for (Tree<T> parent : nodes) {
            String id = parent.getId();
             if (id != null && id.equals(pid)) {
               children.setParent(true);
               parent.setChildren(true);
               parent.getChildrens().add(children);                                         
               continue;
             }
           }
      
         }    
         return topNodes;
     }
}

