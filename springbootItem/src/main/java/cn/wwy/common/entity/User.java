package cn.wwy.common.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户信息类
 * 
 * @author lihy
 *
 */
public class User implements Serializable{

	private int id;
	private String lastModifyUId;

	private Date lastModifyTime;
	private String loginName;

    private String nickName;
	private String name;  //realName
	private String passwd;
    private String phone;
    private String email;
	private String areaId;
	// 0:未激活;1:已激活;-1:删除,
	private String status;
	
	private int onlineStatus;
    private String salt;
    private String isPreinstall;
    private String[] roleIds; //角色数组
    
	private List<Role> roles;

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(int onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getIsPreinstall() {
		return isPreinstall;
	}

	public void setIsPreinstall(String isPreinstall) {
		this.isPreinstall = isPreinstall;
	}

	public String[] getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(String[] roleIds) {
		this.roleIds = roleIds;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastModifyUId() {
		return lastModifyUId;
	}

	public void setLastModifyUId(String lastModifyUId) {
		this.lastModifyUId = lastModifyUId;
	}

	public Date getLastModifyTime() {
		return lastModifyTime;
	}

	public void setLastModifyTime(Date lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}


}
