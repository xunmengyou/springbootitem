/**
 * 
 */
package cn.wwy.common.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangwy
 *
 */
public class BuildTreeNode {
	  public static TreeNode build(List<TreeNode> nodes){
	       if(nodes == null){
	           return null;
	         }
	       List<TreeNode> topNodes = new ArrayList<TreeNode>();
	       
	       for (TreeNode children : nodes) {
	    
	         String pid = children.getPkey();
	         if (pid == null || "".equals(pid)) {
	           topNodes.add(children);
	           continue;
	         }
	    
	         for (TreeNode parent : nodes) {
	        	 
	           String id = parent.getKey();
	           if (id != null && id.equals(pid)) {
	             parent.getChildren().add(children);         
	             continue;
	           }
	         }
	    
	       }
	    
	       TreeNode root = new TreeNode();
	       if (topNodes.size() == 0) {
	         root = topNodes.get(0);
	       } else {
	        //
	       }    
	       return root;
	   }
	  
	  
	  public static List<TreeNode> buildList(List<TreeNode> nodes) {
	       
	       if(nodes == null){
	           return null;
	         }
	       List<TreeNode> topNodes = new ArrayList<TreeNode>();
	       
	       for (TreeNode children : nodes) {
	    
	         String pid = children.getPkey();
	         if (pid == null || "".equals(pid)) {
	           topNodes.add(children);
	           continue;
	         }
	    
	         for (TreeNode parent : nodes) {
	        	 
	           String id = parent.getKey();
	           if (id != null && id.equals(pid)) {
	             parent.getChildren().add(children);         
	             continue;
	           }
	         }
	    
	       }
	       
	       return topNodes;
	     }
}
