/**
 * 
 */
package cn.wwy.common.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wangwy
 *
 */
public class TreeNode {

	private String title; //name
	private String name;   //导航需要的名
	private String key;  //id
	private String pkey; //父ID
	private int seq;  //sort排序
	private boolean isModule = false;  //是否导航，默认不导航(type)
	private String icon;  
	private String path; //url
	private boolean isSelectable = false;  //节点被点击
//	private boolean isSelected  = false;  ; //是否已勾选
	private boolean isDisableCheckbox = false;  //是否有授权(can_authorize)
	private List<TreeNode> children = new ArrayList<TreeNode>(); //节点的子节点
	public String getTitle() {
		return title;
	}
	public String getPkey() {
		return pkey;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public void setPkey(String pkey) {
		this.pkey = pkey;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public boolean isModule() {
		return isModule;
	}
	public void setModule(boolean isModule) {
		this.isModule = isModule;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public boolean isSelectable() {
		return isSelectable;
	}
	public void setSelectable(boolean isSelectable) {
		this.isSelectable = isSelectable;
	}
	public boolean isDisableCheckbox() {
		return isDisableCheckbox;
	}
	public void setDisableCheckbox(boolean isDisableCheckbox) {
		this.isDisableCheckbox = isDisableCheckbox;
	}
	public List<TreeNode> getChildren() {
		return children;
	}
	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}
	
	
}
