
/**  
 * @Title:  ReturnResult.java   
 * @Package com.infinova.oms.common.entity   
 * @Description:	TODO(用一句话描述该文件做什么)   
 * @author: wangwy     
 * @date:   2018年5月31日 下午2:24:47   
 */
  
package cn.wwy.common.entity;  
  

 /**  
 * 类功能简述:	〈一句话）
 * 类功能详述:	返回给前台的数据结构
 * @author:	wangwy  
 * @date:	2018年5月31日 下午2:24:47 
 */

public class ReturnResult
{
    /**
     * 状态码
     */
    private String code;
    /**
     * 提示信息
     */
    private String msg;
    /**
     * 数据，可能是List、Map、实体类、字符串等等
     */
    private Object data;
    public String getCode()
    {
    
        return code;
    }
    public void setCode(String code)
    {
    
        this.code = code;
    }
    public String getMsg()
    {
    
        return msg;
    }
    public void setMsg(String msg)
    {
    
        this.msg = msg;
    }
    public Object getData()
    {
    
        return data;
    }
    public void setData(Object data)
    {
    
        this.data = data;
    }
}

