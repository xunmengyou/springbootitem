
/**  
 * @Title:  UserInfo.java   
 * @Package com.infinova.oms.common.entity   
 * @Description:	TODO(用一句话描述该文件做什么)   
 * @author: wangwy     
 * @date:   2018年6月4日 上午9:17:44   
 */
  
package cn.wwy.common.entity;

import java.io.Serializable;
import java.util.List;

/**  
 * 类功能简述:	〈一句话）
 * 类功能详述:	与数据库User表对应
 * @author:	wangwy  
 * @date:	2018年6月4日 上午9:17:44 
 */

public class UserInfo implements Serializable
{
    private String id;
    private String lastUserId;
    private String lastTime;
    private String lastLoginTime;
    private String loginName;
    private String nickName;
    private String realName;
    private String passwd;
    private String phone;
    private String email;
    private String areaId;
    private String status;
    private String salt;
    private String isPreinstall;
    private String[] roleIds; //角色数组
 
    
    @Override
    public String toString()
    {
        return String.format("【用户id:%s,用户名:%s】", id,loginName);
    }
    public String getId()
    {
    
        return id;
    }
    public void setId(String id)
    {
    
        this.id = id;
    }
    public String getLastUserId()
    {
    
        return lastUserId;
    }
    public void setLastUserId(String lastUserId)
    {
    
        this.lastUserId = lastUserId;
    }
    public String getLastTime()
    {
    
        return lastTime;
    }
    public void setLastTime(String lastTime)
    {
    
        this.lastTime = lastTime;
    }
    public String getLastLoginTime()
    {
    
        return lastLoginTime;
    }
    public void setLastLoginTime(String lastLoginTime)
    {
    
        this.lastLoginTime = lastLoginTime;
    }
    public String getLoginName()
    {
    
        return loginName;
    }
    public void setLoginName(String loginName)
    {
    
        this.loginName = loginName;
    }
    public String getNickName()
    {
    
        return nickName;
    }
    public void setNickName(String nickName)
    {
    
        this.nickName = nickName;
    }
    public String getRealName()
    {
    
        return realName;
    }
    public void setRealName(String realName)
    {
    
        this.realName = realName;
    }
    public String getPasswd()
    {
    
        return passwd;
    }
    public void setPasswd(String passwd)
    {
    
        this.passwd = passwd;
    }
    public String getPhone()
    {
    
        return phone;
    }
    public void setPhone(String phone)
    {
    
        this.phone = phone;
    }
    public String getEmail()
    {
    
        return email;
    }
    public void setEmail(String email)
    {
    
        this.email = email;
    }
    public String getAreaId()
    {
    
        return areaId;
    }
    public void setAreaId(String areaId)
    {
    
        this.areaId = areaId;
    }
    public String getStatus()
    {
    
        return status;
    }
    public void setStatus(String status)
    {
    
        this.status = status;
    }
    public String getSalt()
    {
    
        return salt;
    }
    public void setSalt(String salt)
    {
    
        this.salt = salt;
    }
    public String getIsPreinstall()
    {
    
        return isPreinstall;
    }
    public void setIsPreinstall(String isPreinstall)
    {
    
        this.isPreinstall = isPreinstall;
    }
    public String[] getRoleIds()
    {
    
        return roleIds;
    }
    public void setRoleIds(String[] roleIds)
    {
    
        this.roleIds = roleIds;
    }

}

