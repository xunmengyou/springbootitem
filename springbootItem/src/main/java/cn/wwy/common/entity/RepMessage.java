package cn.wwy.common.entity;


/**
 * 请求后返回给前端的信息
 * 
 * @author lihy
 *
 * @param <T>
 */
public class RepMessage<T> {

	private String state;

	private String message;

	private T result;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

}
