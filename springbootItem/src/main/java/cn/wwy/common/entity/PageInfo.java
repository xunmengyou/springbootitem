package cn.wwy.common.entity;

import java.util.LinkedHashMap;

public class PageInfo {
	
	//当前页
	private int pageNo;
	//页大小
	private int pageSize;
	private int totalCount;
	
	//排序规则
	private LinkedHashMap<String, String> sort;

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public LinkedHashMap<String, String> getSort() {
		return sort;
	}

	public void setSort(LinkedHashMap<String, String> sort) {
		this.sort = sort;
	}
	
	

}
