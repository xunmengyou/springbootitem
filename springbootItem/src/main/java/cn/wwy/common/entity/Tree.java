
/**  
 * @Title:  Tree.java   
 * @Package com.infinova.oms.common.entity   
 * @Description:	TODO(用一句话描述该文件做什么)   
 * @author: wangwy     
 * @date:   2018年6月1日 上午10:28:25   
 */
  
package cn.wwy.common.entity;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;


/**
 * 
 * 类功能简述:	〈一句话）
 * 类功能详述:	
 * @author:	wangwy  
 * @date:	2018年6月1日 上午10:42:50
 */
public class Tree<T>
{
    private String id;
    private String parentId; //父ID
    private T nodeInfo;  //这个节点具体信息
    private List<Tree<T>> childrens = new ArrayList<Tree<T>>(); //节点的子节点
    private boolean isParent = false;  //是否有父节点
    private boolean isChildren = false;  //是否有子节点
    
    public Tree() {
        super();
    }
   
    public Tree(String id,String pid,T node,List<Tree<T>> childrens,
                boolean isParent,boolean isChildren) {
        super();
        this.id = id;
        this.parentId = pid;
        this.nodeInfo = node;
        this.childrens = childrens;
        this.isParent = isParent;
         this.isChildren = isChildren;
    }
    
    @Override
    public String toString() {
       
      return JSON.toJSONString(this);
    }

    public String getId()
    {
    
        return id;
    }

    public void setId(String id)
    {
    
        this.id = id;
    }

    public String getParentId()
    {
    
        return parentId;
    }

    public void setParentId(String parentId)
    {
    
        this.parentId = parentId;
    }

    public T getNodeInfo()
    {
    
        return nodeInfo;
    }

    public void setNodeInfo(T nodeInfo)
    {
    
        this.nodeInfo = nodeInfo;
    }

    public List<Tree<T>> getChildrens()
    {
    
        return childrens;
    }

    public void setChildrens(List<Tree<T>> childrens)
    {
    
        this.childrens = childrens;
    }

    public boolean isParent()
    {
    
        return isParent;
    }

    public void setParent(boolean isParent)
    {
    
        this.isParent = isParent;
    }

    public boolean isChildren()
    {
    
        return isChildren;
    }

    public void setChildren(boolean isChildren)
    {
    
        this.isChildren = isChildren;
    }
}

