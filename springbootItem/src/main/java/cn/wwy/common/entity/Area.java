package cn.wwy.common.entity;

import java.util.Date;

public class Area {

	private int id;
	private String lastModifyUId;

	private Date lastModifyTime;
	private String name;
	// 区域编码
	private String areaCode;
	//级别
	private int level;
	//父区域id
	private String pId;
	private String status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastModifyUId() {
		return lastModifyUId;
	}

	public void setLastModifyUId(String lastModifyUId) {
		this.lastModifyUId = lastModifyUId;
	}

	public Date getLastModifyTime() {
		return lastModifyTime;
	}

	public void setLastModifyTime(Date lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getpId() {
		return pId;
	}

	public void setpId(String pId) {
		this.pId = pId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
