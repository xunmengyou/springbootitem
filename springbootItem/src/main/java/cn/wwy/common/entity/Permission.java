package cn.wwy.common.entity;

import java.io.Serializable;
import java.util.Date;

public class Permission implements Serializable{

	private int id;
	private String lastModifyUId;

	private Date lastModifyTime;
	// 是否页面导航
	private int isModule;
	// 模块Id
	private int moduleId;
	// 父模块Id
	private int pId;
	// 模块层级
	private int moduleLevel;
	// 0:不可删除,系统内置;1:url访问权限;2:页面操作权限;3:有页面操作和url访问权限
	private int type;

	// 权限名或者模块名
	private String name;
	private String description;

	private String url;
	private int seq;
	// 操作Key,例:页面按钮class=operation,Id=op10001的按钮可见
	private String operationKey;
    private String menuType; //是导航还是菜单
	/**
	 * by wwy,2018/06/25
	 */
    private int canAuthorize; //是否可授权 
    
	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public int getCanAuthorize() {
		return canAuthorize;
	}

	public void setCanAuthorize(int canAuthorize) {
		this.canAuthorize = canAuthorize;
	}
	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLastModifyUId() {
		return lastModifyUId;
	}

	public void setLastModifyUId(String lastModifyUId) {
		this.lastModifyUId = lastModifyUId;
	}

	public Date getLastModifyTime() {
		return lastModifyTime;
	}

	public void setLastModifyTime(Date lastModifyTime) {
		this.lastModifyTime = lastModifyTime;
	}

	public int getIsModule() {
		return isModule;
	}

	public void setIsModule(int isModule) {
		this.isModule = isModule;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getpId() {
		return pId;
	}

	public void setpId(int pId) {
		this.pId = pId;
	}

	public int getModuleLevel() {
		return moduleLevel;
	}

	public void setModuleLevel(int moduleLevel) {
		this.moduleLevel = moduleLevel;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getOperationKey() {
		return operationKey;
	}

	public void setOperationKey(String operationKey) {
		this.operationKey = operationKey;
	}

}
