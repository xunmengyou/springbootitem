
/**  
 * @Title:  PermissionResult.java   
 * @Package com.infinova.oms.common.entity   
 * @Description:	TODO(用一句话描述该文件做什么)   
 * @author: wangwy     
 * @date:   2018年6月26日 下午2:27:48   
 */
  
package cn.wwy.common.entity;

import java.util.List;

/**  
 * 类功能简述:	〈一句话）
 * 类功能详述:	
 * @author:	wangwy  
 * @date:	2018年6月26日 下午2:27:48 
 */

public class PermissionResult
{
    private List<TreeNode> tree;
    private  Integer[] selectedList;
    public List<TreeNode> getTree()
    {
    
        return tree;
    }
    public void setTree(List<TreeNode> tree)
    {
    
        this.tree = tree;
    }
    public Integer[] getSelectedList()
    {
    
        return selectedList;
    }
    public void setSelectedList(Integer[] selectedList)
    {
    
        this.selectedList = selectedList;
    }

}

