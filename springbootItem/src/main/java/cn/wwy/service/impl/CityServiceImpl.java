/**
 * 
 */
package cn.wwy.service.impl;

import cn.wwy.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.wwy.dao.CityMapper;
import cn.wwy.domain.City;

/**
 * @author wangwy
 *
 */

@Service
public class CityServiceImpl implements CityService {
	
	@Autowired 
	private CityMapper cityMapper;


	@Override
	public City findByName(String cityName) {
		// TODO Auto-generated method stub
		return cityMapper.findByName(cityName);
	}

}
