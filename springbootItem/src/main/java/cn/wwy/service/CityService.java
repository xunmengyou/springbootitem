/**
 * 
 */
package cn.wwy.service;

import cn.wwy.domain.City;

/**
 * @author wangwy
 *
 */
public interface CityService {
	//根据城市名称，查询城市信息
	City findByName(String cityName);
}
