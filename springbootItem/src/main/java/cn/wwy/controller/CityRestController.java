/**
 * 
 */
package cn.wwy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.wwy.domain.City;
import cn.wwy.service.CityService;

/**
 * @author wangwy
 *
 */

@RestController
public class CityRestController {
	@Autowired 
   private CityService cityService; 

	//http://localhost:8080/api/city?cityName=nanjing
    @RequestMapping(value = "/api/city", method = RequestMethod.GET) 
    //SpringMvc后台进行获取数据,通过@RequestParam注解注入
	public City findOneCity(@RequestParam(value = "cityName", required = true) String cityName) { 
	     return cityService.findByName(cityName);
    } 
}
